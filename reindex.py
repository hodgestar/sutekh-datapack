"""
Script for re-indexing the Sutekh data packs.
"""
from __future__ import print_function

__version__ = "0.1.0"

import datetime
import json
import hashlib
import optparse
import os
import re
import sys

if sys.version_info.major < 3:
    string_type = basestring
else:
    string_type = str


class Index(object):
    """
    A datapack index.
    """

    def __init__(self, filename):
        self.filename = filename
        self.data = None

    def exists(self):
        return os.path.exists(self.filename)

    def initialize(self):
        self.data = {
            "format": "sutekh-datapack",
            "format-version": "1.0",
            "datapacks": [],
        }

    def check(self):
        self.check_field(self, "format", "sutekh-datapack")
        self.check_field(self, "format-version", "1.0")
        self.check_field(self, "datapacks", value_type=list)
        for pack in self["datapacks"]:
            self.check_pack(pack)

    def check_pack(self, pack):
        assert isinstance(pack, dict)
        self.check_field(pack, "file", value_type=string_type)
        self.check_field(pack, "description", value_type=string_type)
        self.check_field(pack, "tag", value_type=string_type)
        self.check_field(pack, "updated_at", value_type=string_type)
        self.check_field(pack, "sha256", value_type=string_type)

    def check_field(self, obj, name, value=None, value_type=None):
        assert name in obj, "%s not in %s" % (name, obj)
        if value is not None:
            assert obj[name] == value, "%s != to %s" % (obj[name], value)
        if value_type is not None:
            assert isinstance(obj[name], value_type), "%s is not of %s" % (obj[name], value_type)

    def read(self):
        with open(self.filename) as f:
            self.data = json.load(f)

    def write(self):
        with open(self.filename, "w") as f:
            json.dump(self.data, f, sort_keys=True, indent=4)

    def format_now(self):
        return datetime.datetime.utcnow().isoformat()

    def format_sha256(self, filename):
        filepath = self.pack_path(filename)
        with open(filepath, 'rb') as f:
            data = f.read()
        return hashlib.sha256(data).hexdigest()

    def __contains__(self, name):
        return name in self.data

    def __getitem__(self, name):
        return self.data[name]

    def pack_path(self, filename):
        index_dir = os.path.dirname(self.filename)
        return os.path.join(index_dir, filename)

    def find_pack(self, filename):
        for pack in self["datapacks"]:
            if pack["file"] == filename:
                return pack
        return None

    def has_pack(self, filename):
        return self.find_pack(filename) is not None

    def update_pack(self, filename):
        pack = self.find_pack(filename)
        assert pack is not None
        pack_sha = self.format_sha256(pack["file"])
        if not 'sha256' in pack or pack_sha != pack['sha256']:
            pack["updated_at"] = self.format_now()
            pack["sha256"] = self.format_sha256(pack["file"])
        self.check_pack(pack)

    def add_pack(self, filename=None, description=None, tag=None):
        pack = {
            "file": filename,
            "description": description,
            "tag": tag,
        }
        self["datapacks"].append(pack)
        self.update_pack(filename)


class Datapack(object):
    """
    A datapack folder.
    """

    IGNORE_DIR_RE = re.compile(r"^\.")
    ACCEPT_FILE_RE = re.compile(r"^.*\.zip$")

    def __init__(self, folder, basedir):
        self.folder = folder
        self.basedir = basedir

    def dirname_okay(self, f):
        return not self.IGNORE_DIR_RE.match(f)

    def filename_okay(self, f):
        return self.ACCEPT_FILE_RE.match(f)

    def walk(self):
        for dirname, dirnames, filenames in os.walk(self.folder):
            dirnames[:] = [f for f in dirnames if self.dirname_okay(f)]
            filenames[:] = [f for f in filenames if self.filename_okay(f)]
            for filename in filenames:
                # We canonicalise to be a relative path to index.json
                yield os.path.relpath(os.path.join(dirname, filename),
                                      self.basedir)


def parse_options(args):
    """Handle the command line options"""
    parser = optparse.OptionParser(usage="usage: %prog [options]",
                                   version="%%prog %s" % __version__)
    parser.add_option(
        "-d", "--datapack-folder",
        type="string", dest="datapack_folder", default=".",
        help="Folder containing the data packs [.]")
    parser.add_option(
        "-i", "--index",
        type="string", dest="index_file", default="index.json",
        help="Name of the JSON index file to generate [index.json]")

    return parser, parser.parse_args(args)


def prompt_pack(filename):
    """Prompt for details for a new pack."""
    print("New pack [%s]:" % (filename,))
    tag = input("Tag? ")
    description = input("Description? ")
    return {
        "tag": tag,
        "description": description,
    }


def main(args):
    """Perform the index update."""
    parser, (opts, args) = parse_options(args)

    if len(args) != 1:
        parser.print_help()
        return 1

    index = Index(opts.index_file)
    basedir = os.path.dirname(os.path.abspath(opts.index_file))
    if not index.exists():
        print("Creating new index [%s]." % (index.filename,))
        index.initialize()
    else:
        print("Reading index [%s]." % (index.filename,))
        index.read()
        index.check()

    datapack = Datapack(opts.datapack_folder, basedir)
    print("Reindexing [%s]" % (datapack.folder,))

    for datafile in datapack.walk():
        if index.has_pack(datafile):
            index.update_pack(datafile)
        else:
            pack = prompt_pack(datafile)
            index.add_pack(filename=datafile, **pack)

    index.write()

    print("Done.")
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
